Role firewalld
==============

Install and configure firewalld.

Config options:
* activation/désactivaction de firewalld
* Creation ipset
* interface of a zone
* source of a zone
* service rules
* port rules
* rich rules

Par défaut, sous CentOS 7, l'interface est dans la zone public avec le service ssh et dhcpv6-client activés.  
Par défaut, avec ce rôle, le service dhcpv6-client est désactivé.

:warning: Ajoute des services/rules/zones/etc. mais n'enleve ces services que s'ils sont explicitement désactivés (*state: disabled*).




Sources
-------

* Beaucoup:   
https://galaxy.ansible.com/FlatKey/firewalld/  
License: MIT  


* Un peu:  
https://galaxy.ansible.com/memiah/firewalld/  
Licence: MIT/BSD  




Role Variables
--------------

**It is not necessary to use all these variable blocks, you can use only the config options you really need.** 


The following variable is used to install and enable firewalld:

```
firewalld_enabled: True
```

---

The following variables are used to define ipset:

```
    firewalld_ipset:
      - name: "nfsclient"
        short: "NFS Clients"
        description: "Limit NFS access to this entries"
        entries:
          - 10.71.10.1
          - 10.71.10.2
          - 10.71.10.3
```

---

The following variables are used to define the source of a zone:

```
    firewalld_zone_source:
      public:
        source: (required, e.g. "192.168.1.0/24")
        state: (optional, only values: enabled|disabled, default: enabled)
        permanent: (optional, only values: true|false, default: true)
        immediate: (optional, only values: true|false, default: true)
```

---

The following variables are used to define a service rule: 

```
    firewalld_service_rules: 
      service:
        state: (optional, only values: enabled|disabled, default: enabled)
        zone: (optional, default: public) 
        permanent: (optional, only values: true|false, default: true)
        immediate: (optional, only values: true|false, default: true)
```

---

The following variables are used to define a port rule: 

```
    firewalld_port_rules: 
      name:
        port: (required, port or port range)
        protocol: (optional, only values: tcp|udp, default: tcp)
        state: (optional, only values: enabled|disabled, default: enabled)
        zone: (optional, default: public)
        permanent: (optional, only values: true|false, default: true)
        immediate: (optional, only values: true|false, default: true)
```

---

The following variables are used to define a rich rule: 

```
    firewalld_rich_rules: 
      name:
        rule: (required, a complete rule in firewalld rich language)
        state: (optional, only values: enabled|disabled, default: enabled)
        zone: (optional, default: public)
        permanent: (optional, only values: true|false, default: true)
        immediate: (optional, only values: true|false, default: true)
```


Handlers
--------

These are the handlers that are defined in this role:

* restart firewalld



Example Playbook
----------------

```yaml
    - hosts: server
      roles:
        - firewalld
      vars:
        # Firewall
        firewalld_enabled: true
        firewalld_service_rules:
          ssh:                    # SSH activé par défaut  
            state: disabled
          dhcpv6-client:          # Activé par défaut
            state: disabled
          nfs:                    # NFSv4, NFSv3
            state: enabled
          rpc-bind:               # NFSv3
            state: enabled
          samba:                  # Samba
            state: enabled
        firewalld_rich_rules:
          ssh-adrmaster:
            rule: 'rule family="ipv4" service name="ssh" source address="147.100.179.16/32" accept'
            state: enabled
          munin-node:
            rule: 'rule family="ipv4" port port="4949" protocol="tcp" source address="147.100.179.16/32" accept'
            state: enabled

```



